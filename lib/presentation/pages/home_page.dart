import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:informationtool/app_router.gr.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/presentation/elements/custom_app_bar.dart';
import 'package:informationtool/presentation/elements/bottom_nav_bar/custom_bottom_bar.dart';
import 'package:informationtool/toolbox/context_extensions.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePage extends StatelessWidget {
  final log = getLogger();
  HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar.iconOnly(
        const Icon(Icons.build),
        () => _navigateToSettingsPage(context),
      ),
      body: Column(
        children: [
          Flexible(child: _buildTitleText(context)),
          Flexible(child: _buildDZLogo(context)),
          Flexible(child: _buildStartButton(context)),
        ],
      ),
      bottomNavigationBar: CustomBottomBar(),
    );
  }

  Widget _buildDZLogo(BuildContext context) {
    return Align(
      child: IconButton(
        onPressed: _navigateToDZ,
        iconSize: 400,
        icon: SvgPicture.asset(
          "assets/dz_logo.svg",
          semanticsLabel: "DZ",
        ),
      ),
    );
  }

  Widget _buildTitleText(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Text(
          context.loc.ai,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: fontXLarge,
          ),
        ),
        Text(
          context.loc.home_info_tool,
          style: const TextStyle(fontSize: fontLarge),
        )
      ],
    );
  }

  Widget _buildStartButton(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: ElevatedButton(
        onPressed: () => _navigateToScenarioSelection(context),
        child: Text(
          context.loc.home_start,
          style: const TextStyle(
            fontSize: fontLarge,
          ),
        ),
      ),
    );
  }

  void _navigateToScenarioSelection(BuildContext context) {
    context.router.push(const ScenarioSelectionRoute());
  }

  void _navigateToSettingsPage(BuildContext context) {
    context.router.push(const SettingsRoute());
  }

  Future<void> _navigateToDZ() async {
    final Uri url = Uri.parse('https://digitalisierungszentrum-uab.de/');
    if (!await launchUrl(url)) {
      log.e('Could not launch $url');
    }
  }
}
