import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';

class BottomBarImage extends StatelessWidget {
  late final String _imagePath;

  late final double _imageFactor;

  BottomBarImage(this._imagePath) {
    this._imageFactor = 1;
  }

  BottomBarImage.withMethod(this._imagePath, this._imageFactor) {
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Image.asset(
      _imagePath,
      height: screenSize.height * footerImageSizeFactor * _imageFactor,
    );
  }
}
