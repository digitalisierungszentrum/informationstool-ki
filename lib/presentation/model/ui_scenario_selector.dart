import 'package:freezed_annotation/freezed_annotation.dart';

part 'ui_scenario_selector.freezed.dart';

@Freezed(makeCollectionsUnmodifiable: false)
class UiScenarioSelector with _$UiScenarioSelector {
  factory UiScenarioSelector({
    required String name,
    required Map<String, bool> values,
  }) = _UiScenarioSelector;
}
