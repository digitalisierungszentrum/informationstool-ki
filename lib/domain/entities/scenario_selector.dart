import 'package:freezed_annotation/freezed_annotation.dart';

part 'scenario_selector.freezed.dart';
part 'scenario_selector.g.dart';

@freezed
class ScenarioSelector with _$ScenarioSelector {
  factory ScenarioSelector({
    required String name,
    required List<String> values,
  }) = _ScenarioSelector;

  factory ScenarioSelector.fromJson(Map<String, dynamic> json) => _$ScenarioSelectorFromJson(json);
}
