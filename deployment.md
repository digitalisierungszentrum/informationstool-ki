# Auslieferung der Webseite

Dieses Dokument dient als Anleitung für die Auslieferung und lokale Ausführung des Informationstools.

## Lokale Ausführung

### Vorbereitungen

Um die Funktionalität der Anwendung testen zu können, kann die Anwendung lokal ausgeführt werden.
Hierfür müssen folgende Befehle in einer Console eingegeben werden.

``` flutter pub get ```

Vor der erstmaligen Ausführung und nach Änderungen an den Abhängigkeiten in der Datei 'pubspec.yaml' müssen die benutzten Bibliotheken heruntergeladen werden.

``` flutter pub run build_runner build ```

Mit diesem Befehl werden die generierten Dateien erstellt. Nach Änderungen am Code werden hierdurch die generierten ```.g.dart``` und ```.freezed.dart``` Dateien angepasst.

### Anwendung ausführen

``` flutter run ```

``` flutter run -d chrome ```

Mit diesen Befehlen wird der lokale Webserver gestartet. Die Anwendung wird daraufhin in einem Browserfenster geöffnet. Mit der Option ``` -d chrome ``` kann der Browser (Chrome oder Edge), welcher genutzt werden soll definiert werden.

### Mögliche Probleme

#### CORS policy Fehler

Bei der lokalen Ausführung des Informationstools kann es sein, dass externe JSON-Dateien mit Szenarien nicht geladen werden, weil ein Problem mit der CORS policy vorliegt.

In dem Stackoverflow-Eintrag <https://stackoverflow.com/questions/65630743/how-to-solve-flutter-web-api-cors-error-only-with-dart-code> wird eine Lösung für diesen Fall gezeigt.

Die angegebene Lösung ist:

- In ``` {Flutter Pfad}\flutter\bin\cache ``` die Datei ``` flutter_tools.stamp ``` zu löschen
- In ``` {Flutter Pfad}\flutter\packages\flutter_tools\lib\src\web ``` die Datei ``` chrome.dart ``` zu öffnen und unter dem Eintrag ``` '--disable-extensions,' ``` den Eintrag ``` '--disable-web-security,' ``` hinzuzufügen (Bitte das KOMMA nicht vergessen).

Flutter ist unter Windows üblicherweise unter ``` C:\src\flutter ``` zu finden.

## Webseite ausliefern

### Vorbereitung

``` flutter pub get ```

``` flutter pub run build_runner build ```

Auch vor der Auslieferung müssen zunächst die benötigten Bibliotheken heruntergeladen und die ```.g.dart``` und ```.freezed.dart``` Dateien generiert werden.

### Auslieferung

``` flutter build web ```

Ist dies geschehen, kann über den Befehl ``` flutter build web ``` der Release erstellt werden. Dies erstellt im Verzeichnis ```./build/web``` das Deployment.

Dieses kann dann über einen Webserver genutzt werden.

Hierfür reicht es zum Beispiel auf einem System mit python den Befehl ``` python -m http.server 8000 ``` in einer Console im Verzeichnis ``` ./build/web ``` auszuführen.

## Docker Container

Für ein einfaches Deployment der Anwendung stehen ein Docker-Container und die Scriptdateien ``` deployment.sh ``` und ``` deployment.bat ``` zur Verfügung.

Nachdem der Webserver manuel erstellt wurde, kann über den Aufruf der ``` docker-compose.yaml ``` ein Webserver in einem Docker-Container gestartet werden.

Um die Anwendung von Grund auf komplett zu erstellen, stehen zwei Scipt-Dateien bereit:

- ``` deployment.sh ``` steht für Linux Systeme zur Verfügung und
- ``` deployment.bat ``` steht für Windows Systeme zur Verfügung

Der Docker-Container stellt die Anwendung auf dem port 8000 zur Verfügung.
