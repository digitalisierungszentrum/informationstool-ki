import 'package:dartz/dartz.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/domain/entities/usecase.dart';
import 'package:informationtool/domain/usecases/filter_scenarios_parameter.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class FilterScenariosUsecase implements Usecase<List<Scenario>, FilterScenariosParameter> {

  @override
  Future<Either<Failure, List<Scenario>>> call(FilterScenariosParameter params) {

    final numberOfAllSelectedSelectorValues = _resolveNumberOfAllSelectorValues(params.selectedScenarioSelectors);

    final filteredScenarios = params.allScenarios
        .map((scenario) => scenario.copyWith(
            matchingHitRatio: _calculateMatchingHitRatio(
              scenario.selectors,
              params.selectedScenarioSelectors,
              numberOfAllSelectedSelectorValues,
            ),
          ),
        )
        .where((scenario) => (scenario.matchingHitRatio ?? 0) > 0)
        .toList();

    return Future.value(right(filteredScenarios));
  }

  int _resolveNumberOfAllSelectorValues(List<ScenarioSelector> allSelectors) {
    return allSelectors.expand((element) => element.values).length;
  }

  double _calculateMatchingHitRatio(
    List<ScenarioSelector> scenarioSelectors,
    List<ScenarioSelector> selectedSelectors,
    int numberOfAllSelectorValues,
    ) {

    var numberOfMatchingSelectorValues = 0;

    for (final scenarioSelector in scenarioSelectors) {
      for (final scenarioSelectorValue in scenarioSelector.values) {

        final matchingSelectedSelectors = selectedSelectors.where((selectedSelector) => selectedSelector.name == scenarioSelector.name);

        final matchingSelectedSelectorValues = matchingSelectedSelectors
            .expand((matchingSelectedSelector) => matchingSelectedSelector.values)
            .where((matchingSelectedSelectorValue) => matchingSelectedSelectorValue == scenarioSelectorValue);

        numberOfMatchingSelectorValues += matchingSelectedSelectorValues.length;
      }
    }

    return numberOfMatchingSelectorValues / numberOfAllSelectorValues;
  }
}
