import 'package:dartz/dartz.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/usecase.dart';
import 'package:informationtool/domain/repositories/scenario_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetAllScenariosUsecase implements Usecase<List<Scenario>, DataSource> {
  final ScenarioRepository repository;

  GetAllScenariosUsecase(this.repository);

  @override
  Future<Either<Failure, List<Scenario>>> call(DataSource params) {
    return params.map(
      local: (_) => repository.getLocalScenarios(),
      remote: (value) => repository.getRemoteScenarios(value.urlToFetch),
    );
  }
}
