import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';

Scenario createScenario(List<ScenarioSelector> selectors, [String? name]) {
  return Scenario(
    name: name ?? "name",
    description: "description",
    links: [],
    picture: "picture",
    selectors: selectors,
  );
}
