part of 'settings_cubit.dart';

@freezed
class SettingsState with _$SettingsState {
  const factory SettingsState.initial({
    @Default(DataSource.local()) DataSource dataSource,
  }) = Initial;
  const factory SettingsState.loading({
    @Default(DataSource.remote("")) DataSource dataSource,
  }) = Loading;
  factory SettingsState.loaded(DataSource dataSource) = Loaded;
  const factory SettingsState.error(String message) = Error;
}
