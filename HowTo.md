# Informationstool KI HowTo

Das Informationstool bietet dem Nutzer die Möglichkeit, anhand eigener Auswahlkriterien passende KI-Anwendungsfälle zu finden und zu erkunden.

![Screenshot Startseite](screenshots/screenshot_web.png)

## Auswahl Datengrundlage

Über das in der rechten oberen Ecke der Startseite befindliche Icon ist es möglich in die Einstellungen der App zu gelangen.

![Screenshot Einstellungsicon](screenshots/screenshot_settings_icon.png)

In diesen ist es möglich, die Quelle der in der App verwendeten Daten zu bestimmen.

![Screenshot Einstellungen](screenshots/screenshot_settings_page.png)

Als Standart wird immer die Json-Datei der Anwendung genommen. Es ist jedoch auch möglich, eine eigene Json-Datei mittels URL als Datengrundlage zu nutzen.

![Screenshot URL Auswahl](screenshots/screenshot_settings_url.png)

Die Struktur der Json-Datei sieht folgendermasen aus:

```json
[
  {
    "name": "<Bezeichnung des Anwendungsfalls>",
    "description": "<Beschreibung des Anwendungsfalls>",
    "links": [
      {
        "name": "<Bezeichnung des Links>",
        "hyperlink": "<Eigentlicher Link>",
        "description": "<Beschreibung des Inhalts des Links>"
      }
    ],
    "picture": "<Bild, das mit dem Anwendungsfall angezeigt werden kann. Bitte die Online-Ressource direkt angeben>",
    "selectors": [
        {
          "name": "<Bezeichnung der Eigenschaft>",
          "values": "<Liste an Ausprägungen dieser Eigenschaft, die zu dem Anwendungsfall passen>"
        }
    ]
  }, 
  . . .
]
```

## Auswahl Szenarios

Per Klick auf den 'Start' Button gelangt man in die Szenarioauswahl. Hier können Auswahlkriterien für Eigenschaften eingegeben werden.

![Screenshot Szenario Auswahl](screenshots/screenshot_scenario_selection.png)

Für die Ergebnisse wird geprüft, welche Anwendungsfälle diese Auswahlkriterien abdecken.

![Screenshot Szenario Ergebnisse](screenshots/screenshot_scenario_results.png)

## Ergebnisanzeige

Wird der Button mit der Anzahl an gefundenen Ergebnissen geklickt, wird die Ergebnisansicht geöffnet. Hier werden alle gefundenen Ergebnisse sortiert nach der Trefferquote angezeigt. 

![Screenshot Ergebnisse](screenshots/screenshot_result_view.png)

Die Trefferquote beschreibt hierbei, wie viele der Ausgewählten Kriterien von den jeweiligen Anwendungsfällen abgedeckt werden. 

Wird auf ein Kärtchen eines Ergebnisses geklickt, so wird eine Detailansicht geöffnet, welche die Informationen des Anwendungsfalles anzeigt. Hier werden neben einer Beschreibung und den Bereichen inklusive Auswahloptionen des Anwendungsfalles auch Links angezeigt, welche weitere Informationen zu den jeweiligen Anwendungsgebieten enthalten.

![Screenshot Ergebnisdetails](screenshots/screenshot_result_details.png)

Klickt man in der Ergebnisanzeige auf das Pfeilicon in der linken oberen Ecke, so gelangt man erneut in die Szenarioauswahl um dort die Eingaben überarbeiten zu können.
