import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/presentation/model/ui_scenario_selector.dart';

/// Converts the ui scenario selectors to the selected domain scenario selectors. Only selected values are returned
List<ScenarioSelector> convertUiSelectorsToSelectedDomainSelectors(
  List<UiScenarioSelector> uiSelectors,
) {
  final List<ScenarioSelector> scenarioSelectors = [];

  for (final element in uiSelectors) {
    if (element.values.values.any((element) => true)) {
      final Map<String, bool> filteredMap = Map.from(element.values)
        ..removeWhere((key, value) => value == false);

      scenarioSelectors.add(
        ScenarioSelector(
          name: element.name,
          values: filteredMap.keys.toList(),
        ),
      );
    }
  }

  return scenarioSelectors;
}

List<UiScenarioSelector> convertDomainSelectorsToUISelectors(
  List<ScenarioSelector> scenarioSelector,
) {
  return scenarioSelector.map((e) {
    final Map<String, bool> optionsMap = {};

    for (final element in e.values) {
      optionsMap[element] = false;
    }

    return UiScenarioSelector(
      name: e.name,
      // sort the selector options
      values: Map.fromEntries(
        optionsMap.entries.toList()
          ..sort(
            (a, b) => a.key.compareTo(b.key),
          ),
      ),
    );
  }).toList();
}
