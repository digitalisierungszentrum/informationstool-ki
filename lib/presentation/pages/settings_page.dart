import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/cubit/settings_cubit.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/presentation/elements/custom_app_bar.dart';
import 'package:informationtool/presentation/elements/bottom_nav_bar/custom_bottom_bar.dart';
import 'package:informationtool/presentation/elements/settings/datasource_selection.dart';
import 'package:informationtool/presentation/utils/url_validator.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class SettingsPage extends StatelessWidget {
  final log = getLogger();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _checkUriScenarioSource(context);
        return true;
      },
      child: GestureDetector(
        onTap: () {
          final FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: Scaffold(
          appBar: CustomAppBar.withMethod(
            context.loc.settings_title,
            () => _returnToStart(context),
          ),
          body: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  context.loc.settings_selection_title,
                  style: const TextStyle(fontSize: fontSmall),
                ),
              ),
              DatasourceSelection(),
              Padding(
                padding: const EdgeInsets.all(10),
                child: ElevatedButton(
                  onPressed: () => _returnToStart(context),
                  child: Text(
                    context.loc.result_back,
                    style: const TextStyle(fontSize: fontSmall),
                  ),
                ),
              )
            ],
          ),
          bottomNavigationBar: CustomBottomBar(),
        ),
      ),
    );
  }

  // if the settings state is loading and no valid url is given, the datasource is changed back to local
  void _checkUriScenarioSource(BuildContext context) {
    final settingsCubit = context.bloc<SettingsCubit>();

    if (settingsCubit.remoteUrl == null ||
        settingsCubit.state is Loading ||
        settingsCubit.state is Error ||
        settingsCubit.remoteUrl!.isEmpty ||
        !validateUrl(settingsCubit.remoteUrl!) ||
        (!settingsCubit.remoteUrl!.endsWith(".json") &&
            !settingsCubit.remoteUrl!.endsWith(".json/"))) {
      settingsCubit.setLocalDataSource();
    }
  }

  void _returnToStart(BuildContext context) {
    _checkUriScenarioSource(context);

    context.router.popUntilRoot();
  }
}
