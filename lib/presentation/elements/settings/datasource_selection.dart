import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:informationtool/cubit/settings_cubit.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/presentation/elements/settings/datasource_url_input.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class DatasourceSelection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, state) {
        return _buildDataSourceSelection(
          context,
          state.map(
            initial: (initial) => initial.dataSource,
            loading: (loading) => loading.dataSource,
            loaded: (loaded) => loaded.dataSource,
            error: (error) => null,
          ),
        );
      },
    );
  }

  Widget _buildDataSourceSelection(
    BuildContext context,
    DataSource? dataSource,
  ) {
    final settingsCubit = context.bloc<SettingsCubit>();

    if (dataSource == null) {
      settingsCubit.setLocalDataSource();
    }

    return Center(
      child: SizedBox(
        width: 700,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300,
              child: Column(
                children: [
                  RadioListTile<DataSource>(
                    title: Text(context.loc.settings_source_predefined),
                    value: const DataSource.local(),
                    groupValue: dataSource,
                    onChanged: (value) {
                      settingsCubit.setLocalDataSource();
                    },
                  ),
                  RadioListTile<DataSource>(
                    title: Text(context.loc.settings_source_url),
                    value: DataSource.remote(
                      dataSource is Remote ? dataSource.urlToFetch : "",
                    ),
                    groupValue: dataSource,
                    onChanged: (value) {
                      settingsCubit.setUserEntryExpected();
                    },
                  ),
                ],
              ),
            ),
            Visibility(
              visible: dataSource is Remote,
              child: UrlInputWidget(initText: settingsCubit.remoteUrl ?? ""),
            ),
          ],
        ),
      ),
    );
  }
}
