FROM python:3.9.13

WORKDIR /app

COPY /build/web convert_scenarios.py scenarios.xlsx ./

# update the scenarios.json file with the current content of the xlsx file
RUN pip install pandas openpyxl \
  && python convert_scenarios.py scenarios.xlsx > assets/assets/scenarios.json
