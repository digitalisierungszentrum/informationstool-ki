import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'selector_page_navigator_state.dart';
part 'selector_page_navigator_cubit.freezed.dart';

@injectable
class SelectorPageNavigatorCubit extends Cubit<SelectorPageNavigatorState> {
  SelectorPageNavigatorCubit()
      : super(const SelectorPageNavigatorState.initial());

  Future<void> updatePageArrows(int index, int numPages) async {
    if (numPages < 2) {
      emit(const SelectorPageNavigatorState.none());
    } else if (index == 0) {
      emit(const SelectorPageNavigatorState.rightOnly());
    } else if (index == (numPages - 1)) {
      emit(const SelectorPageNavigatorState.leftOnly());
    } else {
      emit(const SelectorPageNavigatorState.both());
    }
  }
}
