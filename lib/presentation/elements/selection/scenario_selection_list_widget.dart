import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/cubit/selector_cubit.dart';
import 'package:informationtool/cubit/selector_page_navigator_cubit.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/presentation/elements/selection/scenario_selection_widget.dart';
import 'package:informationtool/presentation/model/ui_scenario_selector.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class ScenarioSelectionListWidget extends StatelessWidget {
  final PageController _pageController;
  final log = getLogger();
  ScenarioSelectionListWidget(this._pageController);

  void _incrementCounter() {
    _pageController.nextPage(
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  void _decrementCounter() {
    _pageController.previousPage(
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
  }

  void _updatePageArrows(int index, BuildContext context) {
    final state = context.bloc<SelectorCubit>().state;
    final selectorPageNavigatorCubit =
        context.bloc<SelectorPageNavigatorCubit>();

    state.map(
      initial: (initial) =>
          {_printNotExpectedSelectorState(initial.runtimeType)},
      loading: (loading) =>
          {_printNotExpectedSelectorState(loading.runtimeType)},
      modifing: (modifiing) =>
          {_printNotExpectedSelectorState(modifiing.runtimeType)},
      loaded: (loaded) => selectorPageNavigatorCubit.updatePageArrows(
        index,
        loaded.uiScenarioSelectors.length,
      ),
      modified: (modified) => selectorPageNavigatorCubit.updatePageArrows(
        index,
        modified.uiScenarioSelectors.length,
      ),
    );
  }

  void _printNotExpectedSelectorState(Type tpye) {
    log.e(
      "The SelectorState $tpye was not expected in the build of the Selection widget",
    );
  }

  List<ScenarioSelectionWidget> _createSelectionWidgets(BuildContext context) {
    final selectorPageNavigatorCubit =
        context.bloc<SelectorPageNavigatorCubit>();
    final state = context.bloc<SelectorCubit>().state;

    return state.map(
      initial: (_) => [],
      loading: (_) => [],
      modifing: (_) => [],
      loaded: (loaded) => _updatePageNavigationAndCreateSelectionList(
        selectorPageNavigatorCubit,
        loaded.uiScenarioSelectors,
      ),
      modified: (modified) => _updatePageNavigationAndCreateSelectionList(
        selectorPageNavigatorCubit,
        modified.uiScenarioSelectors,
      ),
    );
  }

  List<ScenarioSelectionWidget> _updatePageNavigationAndCreateSelectionList(
    SelectorPageNavigatorCubit selectorPageNavigatorCubit,
    List<UiScenarioSelector> uiScenarioSelectors,
  ) {
    if (selectorPageNavigatorCubit.state is Initial) {
      selectorPageNavigatorCubit.updatePageArrows(
        0,
        uiScenarioSelectors.length,
      );
    }

    return uiScenarioSelectors
        .map((e) => ScenarioSelectionWidget(uiScenarioSelector: e))
        .toList();
  }

  bool _decEnabled(BuildContext context) {
    return context.bloc<SelectorPageNavigatorCubit>().state.map(
          initial: (_) => false,
          none: (_) => false,
          both: (_) => true,
          leftOnly: (_) => true,
          rightOnly: (_) => false,
        );
  }

  bool _incEnabled(BuildContext context) {
    return context.bloc<SelectorPageNavigatorCubit>().state.map(
          initial: (_) => false,
          none: (_) => false,
          both: (_) => true,
          leftOnly: (_) => false,
          rightOnly: (_) => true,
        );
  }

  @override
  Widget build(BuildContext context) {
    // This BlocBuilder displays the selectors after the scenarios are loaded
    return BlocBuilder<SelectorCubit, SelectorState>(
      builder: (context, state) =>
          BlocBuilder<SelectorPageNavigatorCubit, SelectorPageNavigatorState>(
        builder: (context, state) => SizedBox(
          width: 800,
          child: Row(
            children: [
              SizedBox(
                height: double.infinity,
                child: IconButton(
                  onPressed: _decEnabled(context) ? _decrementCounter : null,
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    size: fontXLarge,
                  ),
                ),
              ),
              Expanded(
                child: PageView(
                  controller: _pageController,
                  onPageChanged: (value) => _updatePageArrows(value, context),
                  children: _createSelectionWidgets(context),
                ),
              ),
              SizedBox(
                height: double.infinity,
                child: IconButton(
                  onPressed: _incEnabled(context) ? _incrementCounter : null,
                  icon: const Icon(
                    Icons.arrow_forward_ios,
                    size: fontXLarge,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
