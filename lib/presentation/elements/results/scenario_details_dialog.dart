import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/toolbox/context_extensions.dart';
import 'package:informationtool/toolbox/string_extension.dart';
import 'package:url_launcher/url_launcher.dart';

class ScenarioDetailsDialog extends StatelessWidget {
  final log = getLogger();
  final Scenario scenario;

  ScenarioDetailsDialog({super.key, required this.scenario});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Center(
        child: Text(
          scenario.name.capitalize(),
          style: const TextStyle(fontSize: fontSmall - 5),
        ),
      ),
      content: SizedBox(
        width: 600,
        child: ListView(
          children: [
            ConstrainedBox(
              constraints: const BoxConstraints(maxHeight: 500, maxWidth: 500),
              child: Image(
                image: NetworkImage(
                  scenario.picture,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Center(
                child: Text(
                  "${context.loc.result_hit_ratio}: ${(scenario.matchingHitRatio! * 100).toStringAsFixed(2)}%",
                  style: const TextStyle(fontSize: fontSmall - 10),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(scenario.description),
            ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Column(children: _displaySelectors(context)),
            ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(context.loc.result_links),
            ),
            Column(
              children: _displayLinks(context),
            )
          ],
        ),
      ),
      actions: [
        TextButton(
          onPressed: () => context.router.pop(),
          child: Text(context.loc.result_back),
        )
      ],
    );
  }

  List<Widget> _displaySelectors(BuildContext context) {
    return scenario.selectors
        .map(
          (e) => SizedBox(
            width: double.infinity,
            child: Text(
              "${e.name.capitalize()}: ${e.values.join(', ')}",
            ),
          ),
        )
        .toList();
  }

  List<Widget> _displayLinks(BuildContext context) {
    return scenario.links
        .map(
          (e) => Padding(
            padding: const EdgeInsets.only(top: 10),
            child: SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                onPressed: () => _openLink(e.hyperlink),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                child: Column(
                  children: [
                    Text(
                      e.name,
                      style: const TextStyle(fontSize: fontSmall - 5),
                    ),
                    Text(e.description)
                  ],
                ),
              ),
            ),
          ),
        )
        .toList();
  }

  Future<void> _openLink(String link) async {
    final Uri url = Uri.parse(link);
    if (!await launchUrl(url)) {
      log.e('Could not launch $url');
    }
  }
}
