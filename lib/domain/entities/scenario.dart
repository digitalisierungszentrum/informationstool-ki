import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:informationtool/domain/entities/scenario_link.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';

part 'scenario.freezed.dart';
part 'scenario.g.dart';

@freezed
class Scenario with _$Scenario {
  factory Scenario({
    required String name,
    required String description,
    required List<ScenarioLink> links,
    required String picture,
    required List<ScenarioSelector> selectors,
    double? matchingHitRatio,
  }) = _Scenario;

  factory Scenario.fromJson(Map<String, dynamic> json) => _$ScenarioFromJson(json);
}
