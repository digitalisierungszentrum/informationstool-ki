import 'package:flutter_test/flutter_test.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/domain/usecases/filter_scenarios.dart';
import 'package:informationtool/domain/usecases/filter_scenarios_parameter.dart';
import 'package:informationtool/toolbox/either_extensions.dart';

import 'scenario_util.dart';

void main() {
  final scenarios = [
    createScenario(
      [
        ScenarioSelector(name: "selector1", values: ["value1", "value2"]),
        ScenarioSelector(name: "selector3", values: ["value5", "value6"]),
      ],
      "scenario1",
    ),
    createScenario(
      [
        ScenarioSelector(name: "selector1", values: ["value7", "value8"]),
        ScenarioSelector(name: "selector2", values: ["value3", "value4"]),
        ScenarioSelector(name: "selector3", values: ["value5", "value6"]),
      ],
      "scenario2",
    ),
    createScenario(
      [
        ScenarioSelector(name: "selector1", values: ["value1", "value9"]),
      ],
      "scenario3",
    ),
  ];

  test("Is should filter a single selected selector correctly", () async {
    // assign
    final scenarioSelectors = [
      ScenarioSelector(name: "selector1", values: ["value1"])
    ];
    final parameters = FilterScenariosParameter(
      allScenarios: scenarios,
      selectedScenarioSelectors: scenarioSelectors,
      allScenarioSelectors: scenarioSelectors,
    );

    // act
    final data = await FilterScenariosUsecase()(parameters);

    // assert
    expect(data.isRight(), isTrue);
    expect(data.getNullableRight()!.length, 2);
    expect(data.getNullableRight()!.first.name, "scenario1");
    expect(data.getNullableRight()!.last.name, "scenario3");
  });

  test("Is should filter a not existing selected selector correctly", () async {
    // assign
    final scenarioSelectors = [
      ScenarioSelector(name: "selector1", values: ["value10"])
    ];
    final parameters = FilterScenariosParameter(
      allScenarios: scenarios,
      selectedScenarioSelectors: scenarioSelectors,
      allScenarioSelectors: scenarioSelectors,
    );

    // act
    final data = await FilterScenariosUsecase()(parameters);

    // assert
    expect(data.isRight(), isTrue);
    expect(data.getNullableRight()!.length, 0);
  });

  test(
    "Is should filter a multiple selected selectors connected with an or statement correctly",
    () async {
      // assign
      final scenarioSelectors = [
        ScenarioSelector(name: "selector1", values: ["value9"]),
        ScenarioSelector(name: "selector2", values: ["value4"]),
        ScenarioSelector(name: "selector3", values: ["value7"]),
      ];
      final parameters = FilterScenariosParameter(
        allScenarios: scenarios,
        selectedScenarioSelectors: scenarioSelectors,
        allScenarioSelectors: scenarioSelectors,
      );

      // act
      final data = await FilterScenariosUsecase()(parameters);

      // assert
      expect(data.isRight(), isTrue);
      expect(data.getNullableRight()!.length, 2);
      expect(data.getNullableRight()!.first.name, "scenario2");
      expect(data.getNullableRight()!.last.name, "scenario3");
    },
  );

  test(
    "Is should calculate the correct selector matching value",
    () async {
      // assign
      final allScenarioSelectors = [
        ScenarioSelector(name: "selector1", values: ["value9"]),
        ScenarioSelector(name: "selector5", values: ["value4"]),
        ScenarioSelector(name: "selector3", values: ["value7"]),
      ];
      final parameters = FilterScenariosParameter(
        allScenarios: scenarios,
        selectedScenarioSelectors: allScenarioSelectors.sublist(0, 2),
        allScenarioSelectors: allScenarioSelectors,
      );

      // act
      final data = await FilterScenariosUsecase()(parameters);

      // assert
      expect(data.isRight(), isTrue);
      expect(data.getNullableRight()!.length, 1);
      expect(data.getNullableRight()!.first.name, "scenario3");
      expect(data.getNullableRight()!.first.matchingHitRatio, 1/2);
    },
  );
}
