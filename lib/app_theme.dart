import 'package:flutter/material.dart';

const fontXLarge = 60.0;
const fontLarge = 50.0;
const fontMedium = 40.0;
const fontSmall = 30.0;

// spacing between the elements of the footer (horizontal and vertical)
const footerSpacing = 40.0;
// Padding of the footer to the bottom of the page
const footerBottomPadding = 0.03;
// Height of the footer elements (in percent of the complete page height)
const footerImageSizeFactor = 0.1;
// Backgroundcolor of footer
const footerBackgroundColor = Color.fromARGB(255,200,200,200);

const blue1 = Color.fromARGB(255, 0, 179, 240);
const blue2 = Color.fromARGB(255, 60, 80, 122);
const blue3 = Color.fromARGB(255, 102, 181, 228);

final appTheme = ThemeData(
  unselectedWidgetColor: blue2,
  colorScheme: const ColorScheme.light(
    secondary: blue1,
  ),
  appBarTheme: const AppBarTheme(
    backgroundColor: blue1,
    foregroundColor: Colors.white,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      shape: MaterialStateProperty.all<OutlinedBorder>(const StadiumBorder()),
      padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
        const EdgeInsets.all(10),
      ),
      backgroundColor: MaterialStateProperty.all<Color>(blue1),
      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
    ),
  ),
  dividerTheme: const DividerThemeData(
    color: blue2,
    thickness: 3.0,
  ),
  iconTheme: const IconThemeData(
    color: blue2,
  ),
  disabledColor: blue3, // color of disabled IconButton
  textButtonTheme: TextButtonThemeData(
    style: TextButton.styleFrom(foregroundColor: blue1),
  ),
  inputDecorationTheme: InputDecorationTheme(
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: const BorderSide(
        color: blue1,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(
        color: blue2,
      ),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(30),
      borderSide: const BorderSide(
        color: Colors.red,
      ),
    ),
    errorBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(10),
      borderSide: const BorderSide(
        color: Colors.red,
      ),
    ),
  ),
  textSelectionTheme: const TextSelectionThemeData(
    cursorColor: blue1,
    selectionColor: blue1,
    selectionHandleColor: blue1,
  ),
);
