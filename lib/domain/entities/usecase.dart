import 'package:dartz/dartz.dart';
import 'package:informationtool/domain/entities/failure.dart';

abstract class Usecase<Type, Params> {
  Future<Either<Failure, Type>> call(Params params);
}
