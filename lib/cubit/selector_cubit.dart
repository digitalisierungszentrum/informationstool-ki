import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/usecases/filter_scenarios.dart';
import 'package:informationtool/domain/usecases/filter_scenarios_parameter.dart';
import 'package:informationtool/domain/usecases/get_all_scenario_selectors.dart';
import 'package:informationtool/domain/usecases/get_all_scenarios.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/presentation/model/ui_scenario_selector.dart';
import 'package:informationtool/presentation/utils/model_converter.dart';
import 'package:injectable/injectable.dart';

part 'selector_cubit.freezed.dart';
part 'selector_state.dart';

@injectable
class SelectorCubit extends Cubit<SelectorState> {
  final log = getLogger();
  List<Scenario>? _allScenarios;
  List<Scenario>? _filteredScenarios;
  List<UiScenarioSelector>? _uiScenarioSelectors;

  final GetAllScenariosUsecase _scenariosUsecase;
  final GetAllScenarioSelectorsUsecase _scenarioSelectorsUsecase;
  final FilterScenariosUsecase _filterScenariosUsecase;

  SelectorCubit(
    this._scenariosUsecase,
    this._scenarioSelectorsUsecase,
    this._filterScenariosUsecase,
  ) : super(const SelectorState.initial());

  Future<void> loadScenarios(DataSource dataSource) async {
    emit(const SelectorState.loading());
    final result = await _scenariosUsecase.call(dataSource);
    _allScenarios = result.getOrElse(() => []);

    final allSelectorsResult =
        await _scenarioSelectorsUsecase.call(_allScenarios!);
    final allSelectors = allSelectorsResult.getOrElse(() => []);

    // convert the domain model scenario selectors into ui model scenario selectors
    _uiScenarioSelectors = convertDomainSelectorsToUISelectors(allSelectors);

    // get the filtered scenarios (for consistent behavior between start (0 selectors) and all selectors deselected)
    final filteredResult = await _filterScenariosUsecase.call(
      FilterScenariosParameter(
        allScenarios: _allScenarios!,
        selectedScenarioSelectors: [],
        allScenarioSelectors: allSelectors,
      ),
    );
    _filteredScenarios = filteredResult.getOrElse(() => []);

    emit(SelectorState.loaded(_uiScenarioSelectors!, _filteredScenarios!));
  }

  Future<void> toggleSelectionOption(String selector, String option) async {
    emit(const SelectorState.modifing());

    // get the old value of the check box
    final oldValue = _uiScenarioSelectors
        ?.firstWhere((element) => element.name == selector)
        .values[option];

    // toggle the value of the check box
    _uiScenarioSelectors
        ?.firstWhere((element) => element.name == selector)
        .values[option] = !oldValue!;

    final allSelectorsResult =
        await _scenarioSelectorsUsecase.call(_allScenarios!);
    final allScenarioSelectors = allSelectorsResult.getOrElse(() => []);
    final selectedScenarioSelectors =
        convertUiSelectorsToSelectedDomainSelectors(_uiScenarioSelectors!);

    // get the filtered scenarios
    final filteredResult = await _filterScenariosUsecase.call(
      FilterScenariosParameter(
        allScenarios: _allScenarios!,
        selectedScenarioSelectors: selectedScenarioSelectors,
        allScenarioSelectors: allScenarioSelectors,
      ),
    );

    _filteredScenarios = filteredResult.getOrElse(() => []);

    for (final element in _filteredScenarios!) {
      log.d("${element.name}: ${element.matchingHitRatio}");
    }

    emit(SelectorState.modified(_uiScenarioSelectors!, _filteredScenarios!));
  }
}
