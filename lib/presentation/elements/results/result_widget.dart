import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/presentation/elements/results/scenario_details_dialog.dart';
import 'package:informationtool/toolbox/context_extensions.dart';
import 'package:informationtool/toolbox/string_extension.dart';

class ResultWidget extends StatelessWidget {
  final Scenario scenario;

  const ResultWidget({super.key, required this.scenario});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => _showresultDetails(context),
      style: ElevatedButton.styleFrom(
        backgroundColor: blue3,
        padding: const EdgeInsets.all(5),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
      child: IntrinsicHeight(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxHeight: 200),
                child: Image(
                  image: NetworkImage(
                    scenario.picture,
                  ),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  scenario.name.capitalize(),
                  style: const TextStyle(
                    fontSize: fontSmall,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Text(
                  "${context.loc.result_hit_ratio}: ${(scenario.matchingHitRatio! * 100).toStringAsFixed(2)}%",
                  style: const TextStyle(fontSize: fontSmall - 10),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showresultDetails(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => ScenarioDetailsDialog(scenario: scenario),
    );
  }
}
