import 'package:get_it/get_it.dart';
import 'package:informationtool/injection.config.dart';
import 'package:injectable/injectable.dart';

final getIt = GetIt.instance;

@InjectableInit(
  preferRelativeImports: false,
)
void configureDependencies() => $initGetIt(getIt);
