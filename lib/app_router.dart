import 'package:auto_route/auto_route.dart';
import 'package:informationtool/presentation/pages/home_page.dart';
import 'package:informationtool/presentation/pages/result_page.dart';
import 'package:informationtool/presentation/pages/scenario_selection_page.dart';
import 'package:informationtool/presentation/pages/settings_page.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  preferRelativeImports: false,
  routes: <AutoRoute>[
    AutoRoute(page: HomePage, initial: true),
    AutoRoute(page: SettingsPage),
    AutoRoute(page: ScenarioSelectionPage),
    AutoRoute(page: ResultPage)
  ],
)
class $AppRouter {}
