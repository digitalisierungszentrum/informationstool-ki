import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/cubit/selector_cubit.dart';
import 'package:informationtool/presentation/model/ui_scenario_selector.dart';
import 'package:informationtool/toolbox/context_extensions.dart';
import 'package:informationtool/toolbox/string_extension.dart';

class ScenarioSelectionWidget extends StatelessWidget {
  final UiScenarioSelector uiScenarioSelector;

  const ScenarioSelectionWidget({super.key, required this.uiScenarioSelector});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 10, top: 20),
          child: Text(
            uiScenarioSelector.name.capitalize(),
            style: const TextStyle(
              fontSize: fontSmall,
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            width: 400,
            child: ListView(
              shrinkWrap: true,
              padding: const EdgeInsets.all(8),
              children: uiScenarioSelector.values.keys.map((key) {
                return CheckboxListTile(
                  value: uiScenarioSelector.values[key],
                  onChanged: (value) {
                    context.bloc<SelectorCubit>().toggleSelectionOption(
                          uiScenarioSelector.name,
                          key,
                        );
                  },
                  title: Text(key.capitalize()),
                  controlAffinity: ListTileControlAffinity.leading,
                );
              }).toList(),
            ),
          ),
        ),
      ],
    );
  }
}
