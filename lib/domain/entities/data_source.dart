import 'package:freezed_annotation/freezed_annotation.dart';

part 'data_source.freezed.dart';

@freezed
class DataSource with _$DataSource {
  const factory DataSource.local() = Local;
  const factory DataSource.remote(String urlToFetch) = Remote;
}
