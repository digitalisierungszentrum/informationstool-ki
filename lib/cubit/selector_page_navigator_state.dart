part of 'selector_page_navigator_cubit.dart';

@freezed
class SelectorPageNavigatorState with _$SelectorPageNavigatorState {
  const factory SelectorPageNavigatorState.initial() = Initial;
  const factory SelectorPageNavigatorState.none() = None;
  const factory SelectorPageNavigatorState.both() = Both;
  const factory SelectorPageNavigatorState.leftOnly() = LeftOnly;
  const factory SelectorPageNavigatorState.rightOnly() = RightOnly;
}
