# Informationstool KI

Das Informationstool bietet die Möglichkeit KI-Anwendungsfälle zu erkunden.
Der Nutzer kann verschiedene Selektoren wählen und bekommt dazu passende Anwendungsfälle angezeigt.

![screenshot](screenshots/screenshot_web.png)

Durch das Auswählen von Selektoren werden dem Nutzer übereinstimmende Anwendungsfälle mit der
Quote der Übereinstimmung angezeigt.

## Daten

Die Daten, die in der App angezeigt werden, werden aus einer JSON-Datei eingelesen.
Es wird eine Datei in diesem Repository verwaltet (`/assets/scenario.json`), die standardmäßig
vorausgewählt ist und so einige Anwendungsfälle in der App auflistet.
Es ist ebenso möglich, eine JSON Datei per URL als Datengrundlage in der App zu laden.

Beim erstellen eines docker images (siehe deployment.md), wird die Datei `scenarios.xlsx` gelesen,
mittels des scripts `convert_scenarios.py` in das passende JSON-Format umgewandelt und in der
Anwendung gespeichert. D.h. beim Deployment über docker, wird diese Exceldatei als Datengrundlage
für die standardmäßig angezeigten Anwendungsfälle verwendet.

Der Inhalt der Excel-Tabelle ist dabei wie im folgenden Bild zu sehen aufgebaut:

![scenarios_table](scenarios_screenshot.png)

Zum Erstellen einer JSON-Datei aus einer solchen Excel-Tabelle, kann das Script `convert_scenarios.py` auch
direkt ausgeführt werden.

``` python convert_scenarios.py scenarios.xlsx > scenarios.json ```

### Datenstruktur

Die Anwendungsfälle werden in der folgenden JSON-Struktur eingepflegt:

```json
{
    "name": "<Bezeichnung des Anwendungsfalls>",
    "description": "<Beschreibung des Anwendungsfalls>",
    "links": [
      {
        "name": "<Bezeichnung des Links>",
        "hyperlink": "<Eigentlicher Link>",
        "description": "<Beschreibung des Inhalts des Links>"
      }
    ],
    "picture": "<Bild, das mit dem Anwendungsfall angezeigt werden kann. Bitte die Online-Ressource direkt angeben>",
    "selectors": [
        {
          "name": "<Bezeichnung der Eigenschaft>",
          "values": "<Liste an Ausprägungen dieser Eigenschaft, die zu dem Anwendungsfall passen>"
        }
    ]
  }
```

`links` enthält eine Liste von Links, die weitere Informationen zu dem jeweiligen Anwendungsfall
bereitstellen.
`selectors` enthält eine Liste von Selektoren mit beliebigen Namen und Werten, die in der App
dynamisch dargestellt werden.

## Entwicklung

Das Tool ist mit [Flutter](https://flutter.dev/) entwickelt und kann dadurch für verschiedene
Plattformen bereitgestellt werden.
