import 'package:freezed_annotation/freezed_annotation.dart';

part 'scenario_link.freezed.dart';
part 'scenario_link.g.dart';

@freezed
class ScenarioLink with _$ScenarioLink {
  factory ScenarioLink({
    required String name,
    required String hyperlink,
    required String description,
  }) = _ScenarioLink;

  factory ScenarioLink.fromJson(Map<String, dynamic> json) => _$ScenarioLinkFromJson(json);
}
