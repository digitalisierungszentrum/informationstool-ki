import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/presentation/elements/custom_app_bar.dart';
import 'package:informationtool/presentation/elements/bottom_nav_bar/custom_bottom_bar.dart';
import 'package:informationtool/presentation/elements/results/result_widget.dart';
import 'package:informationtool/presentation/elements/return_information.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class ResultPage extends StatelessWidget {
  final List<Scenario> _scenarios;

  const ResultPage(this._scenarios);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context.loc.result_title),
      body: Column(
        children: [
          Expanded(
            child: SizedBox(
              width: 700,
              child: ListView.separated(
                shrinkWrap: true,
                padding: const EdgeInsets.all(10),
                itemCount: _scenarios.length,
                itemBuilder: (context, index) {
                  return ResultWidget(scenario: _scenarios.elementAt(index));
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const Padding(padding: EdgeInsets.all(5));
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () => _returnToStart(context),
                child: Text(
                  context.loc.result_restart,
                  style: const TextStyle(fontSize: fontSmall),
                ),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: CustomBottomBar(),
    );
  }

  void _returnToStart(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => ReturnInformationWidget(),
    );
  }
}
