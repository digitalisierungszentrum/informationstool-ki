part of 'selector_cubit.dart';

@freezed
class SelectorState with _$SelectorState {
  const factory SelectorState.initial() = _Initial;
  const factory SelectorState.loading() = Loading;
  const factory SelectorState.loaded(
    List<UiScenarioSelector> uiScenarioSelectors,
    List<Scenario> scenarios,
  ) = Loaded;
  const factory SelectorState.modifing() = Modifiing;
  const factory SelectorState.modified(
    List<UiScenarioSelector> uiScenarioSelectors,
    List<Scenario> scenarios,
  ) = Modified;
}
