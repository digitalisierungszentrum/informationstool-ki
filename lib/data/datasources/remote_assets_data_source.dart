import 'package:http/http.dart' as http;
import 'package:informationtool/data/datasources/assets_data_source.dart';
import 'package:informationtool/data/models/remote_exception.dart';
import 'package:injectable/injectable.dart';

@named
@LazySingleton(as: AssetsDataSource)
class RemoteAssetsDataSourceImpl implements AssetsDataSource {
  @override
  Future<String> getAssetString(String assetsUrl) async {
    // fix for cors error: https://stackoverflow.com/questions/65630743/how-to-solve-flutter-web-api-cors-error-only-with-dart-code

    final url = Uri.parse(assetsUrl);
    final response = await http.get(url);

    if(response.statusCode == 200){
      return response.body;
    }
    throw RemoteException("error-loading-remote-data");
  }
}
