import 'package:collection/collection.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/domain/usecases/get_all_scenario_selectors.dart';
import 'package:informationtool/toolbox/either_extensions.dart';

import 'scenario_util.dart';


void main() {
  void checkSelectors(
    Either<Failure, List<ScenarioSelector>> data,
    String selectorName,
    List<String> selectorValues,
  ) {
    expect(
      data
          .getNullableRight()!
          .singleWhere((s) => s.name == selectorName)
          .values
          .length,
      selectorValues.length,
    );
    expect(
      data
          .getNullableRight()!
          .singleWhere((s) => s.name == selectorName)
          .values,
      containsAll(selectorValues),
    );
  }

  test("It should return an empty list, if no selectors are given", () async {
    // assign
    final List<Scenario> parameter = [];

    // act
    final data = await GetAllScenarioSelectorsUsecase()(parameter);

    // assert
    expect(data.isRight(), isTrue);
    expect(data.getNullableRight()!.length, 0);
  });

  test("It should return the list of the repository", () async {
    // assign
    final parameter = [
      createScenario([
        ScenarioSelector(name: "selector1", values: ["value1", "value2"]),
        ScenarioSelector(name: "selector2", values: ["value3", "value4"]),
        ScenarioSelector(name: "selector3", values: ["value5", "value6"]),
      ]),
      createScenario([
        ScenarioSelector(name: "selector1", values: ["value1", "value7"]),
        ScenarioSelector(name: "selector2", values: ["value3", "value8"]),
        ScenarioSelector(name: "selector3", values: ["value5", "value9"]),
      ]),
      createScenario([
        ScenarioSelector(name: "selector4", values: ["value10", "value11"]),
      ]),
    ];

    // act
    final data = await GetAllScenarioSelectorsUsecase()(parameter);

    // assert
    expect(data.isRight(), isTrue);
    data
        .getNullableRight()!
        .groupListsBy((s) => s.name)
        .forEach((key, value) => expect(value.length, 1));
    checkSelectors(data, "selector1", ["value1", "value2", "value7"]);
    checkSelectors(data, "selector2", ["value3", "value4", "value8"]);
    checkSelectors(data, "selector3", ["value5", "value6", "value9"]);
    checkSelectors(data, "selector4", ["value10", "value11"]);
  });
}
