import 'package:collection/collection.dart';
import 'package:dartz/dartz.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/domain/entities/usecase.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class GetAllScenarioSelectorsUsecase implements Usecase<List<ScenarioSelector>, List<Scenario>>{
  @override
  Future<Either<Failure, List<ScenarioSelector>>> call(List<Scenario> params) {
    
    final selectors = params
        .expand((s) => s.selectors)
        .groupListsBy((s) => s.name)
        .entries
        .map((e) => ScenarioSelector(
              name: e.key,
              values: e.value.expand((s) => s.values).toSet().toList(),
          ),
        )
        .toList();

    return Future.value(right(selectors));
  }

}
