class RemoteException implements Exception {
  final String errorCode;

  RemoteException(this.errorCode);
}
