import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';

part 'filter_scenarios_parameter.freezed.dart';

@freezed
class FilterScenariosParameter with _$FilterScenariosParameter {
  factory FilterScenariosParameter({
    required List<Scenario> allScenarios,
    required List<ScenarioSelector> selectedScenarioSelectors,
    required List<ScenarioSelector> allScenarioSelectors,
  }) = _FilterScenariosParameter;
}
