// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:informationtool/cubit/settings_cubit.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/presentation/utils/url_validator.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class UrlInputWidget extends StatelessWidget {
  final log = getLogger();
  final String initText;
  late final _controller = TextEditingController(text: initText);

  UrlInputWidget({super.key, required this.initText});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(context.loc.settings_url_title),
        Padding(
          padding: const EdgeInsets.all(10),
          child: BlocBuilder<SettingsCubit, SettingsState>(
            builder: (context, state) {
              return ValueListenableBuilder(
                valueListenable: _controller,
                builder: (context, TextEditingValue value, __) {
                  return TextFormField(
                    decoration: InputDecoration(
                      border: const OutlineInputBorder(),
                      hintText: context.loc.settings_url_hint,
                      errorText: _errorText(context),
                    ),
                    controller: _controller,
                    onTapOutside: (_) => {_setRemoteUrl(context)},
                    onFieldSubmitted: (_) => {_setRemoteUrl(context)},
                  );
                },
              );
            },
          ),
        )
      ],
    );
  }

  String? _errorText(BuildContext context) {
    final value = _controller.value.text;

    if (value.isEmpty) {
      return context.loc.error_url_empty;
    } else if (!validateUrl(value)) {
      return context.loc.error_url_invalid;
    } else if (!value.endsWith(".json") & !value.endsWith(".json/")) {
      return context.loc.error_url_no_json;
    } else {
      return null;
    }
  }

  void _setRemoteUrl(BuildContext context) {
    final remoteUrl = _controller.value.text;

      final settingsCubit = context.bloc<SettingsCubit>();
      settingsCubit.setRemoteUrl(remoteUrl);
  }
}
