@ECHO OFF 
TITLE Informationtool deployment

::Section 1:Preparation
ECHO ============================
ECHO Prepare Informationtool
ECHO ============================

CALL flutter pub get
CALL flutter pub run build_runner build

::Section 2:build web app 
ECHO ============================
ECHO Build Informationtool App
ECHO ============================

CALL flutter build web

::Section 3:Deploy
ECHO ============================
ECHO Deploy Web App in Docker
ECHO ============================

CALL docker compose up --build --force-recreate
