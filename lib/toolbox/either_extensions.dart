import 'package:dartz/dartz.dart';

extension DartzEitherExtensions<R> on Either<dynamic, R>{
  /// Converts an either to a nullable of the right value.
  /// If right has a value, the value will be returned otherwise null.
  R? getNullableRight(){
    return toOption().toNullable();
  }
}
