import 'package:flutter/services.dart';
import 'package:informationtool/data/datasources/assets_data_source.dart';
import 'package:injectable/injectable.dart';

@named
@LazySingleton(as: AssetsDataSource)
class LocalAssetsDataSourceImpl implements AssetsDataSource {
  @override
  Future<String> getAssetString(String assetPath) {
    return rootBundle.loadString(assetPath);
  }
}
