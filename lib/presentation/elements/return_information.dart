import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class ReturnInformationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(context.loc.return_dialog_title),
      content: Text(context.loc.return_dialog_content),
      actions: [
        TextButton(
          onPressed: () => _cancelDialog(context),
          child: Text(context.loc.return_dialog_cancel),
        ),
        TextButton(
          onPressed: () => _returnToStart(context),
          child: Text(context.loc.return_dialog_confirm),
        )
      ],
    );
  }

  void _cancelDialog(BuildContext context) {
    context.router.pop();
  }

  void _returnToStart(BuildContext context) {
    context.router.popUntilRoot();
  }
}
