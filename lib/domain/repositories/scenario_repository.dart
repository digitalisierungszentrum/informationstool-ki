import 'package:dartz/dartz.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';

abstract class ScenarioRepository{
  Future<Either<Failure, List<Scenario>>> getLocalScenarios();
  Future<Either<Failure, List<Scenario>>> getRemoteScenarios(String urlToFetch);
}
