import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

extension LocalizedBuildContext on BuildContext {
  AppLocalizations get loc => AppLocalizations.of(this)!;
}

extension BlocExtension on BuildContext {
  T bloc<T extends Cubit>() {
    return BlocProvider.of<T>(this);
  }
}
