import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:informationtool/data/datasources/assets_data_source.dart';
import 'package:informationtool/data/datasources/local_assets_data_source.dart';
import 'package:informationtool/data/datasources/remote_assets_data_source.dart';
import 'package:informationtool/data/models/remote_exception.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/repositories/scenario_repository.dart';
import 'package:injectable/injectable.dart';

@LazySingleton(as: ScenarioRepository)
class ScenarioRepositoryImpl implements ScenarioRepository {
  final AssetsDataSource localAssets;
  final AssetsDataSource remoteAssets;

  ScenarioRepositoryImpl(
    @Named.from(LocalAssetsDataSourceImpl) this.localAssets,
    @Named.from(RemoteAssetsDataSourceImpl) this.remoteAssets,
  );

  @override
  Future<Either<Failure, List<Scenario>>> getRemoteScenarios(
    String urlToFetch,
  ) async {
    return _getScenarios(
      remoteAssets,
      urlToFetch,
      "error-loading-remote-json",
    );
  }

  @override
  Future<Either<Failure, List<Scenario>>> getLocalScenarios() async {
    return _getScenarios(
      localAssets,
      "assets/scenarios.json",
      "error-loading-local-json",
    );
  }

  Future<Either<Failure, List<Scenario>>> _getScenarios(
    AssetsDataSource dataSource,
    String assetPath,
    String errorCode,
  ) async {
    try {
      final scenariosString = await dataSource.getAssetString(assetPath);
      return right(_convertToObject(scenariosString));
    } on RemoteException catch (e) {
      return left(Failure(e.errorCode));
    } catch (e) {
      return left(Failure(errorCode));
    }
  }

  List<Scenario> _convertToObject(String scenariosString) {
    final scenariosJson = jsonDecode(scenariosString) as List;
    return scenariosJson
        .map((s) => Scenario.fromJson(s as Map<String, dynamic>))
        .toList();
  }
}
