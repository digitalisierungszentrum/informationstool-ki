import 'package:flutter/material.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/presentation/elements/bottom_nav_bar/bottom_nav_bar_image.dart';

class CustomBottomBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return ColoredBox(
      color: footerBackgroundColor,
      child: Padding(
        padding: EdgeInsets.only(
            bottom: screenSize.height * footerBottomPadding,
            top: screenSize.height * footerBottomPadding),
        child: Wrap(
          spacing: footerSpacing,
          runSpacing: footerSpacing,
          alignment: WrapAlignment.center,
          runAlignment: WrapAlignment.center,
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [
            BottomBarImage("assets/IW-RGB.png"),
            BottomBarImage.withMethod("assets/Digital_Laend_logo_RGB.png", 0.5),
            BottomBarImage("assets/BW100_GR_2C_Ministerien_MWAT.png"),
          ],
        ),
      ),
    );
  }
}
