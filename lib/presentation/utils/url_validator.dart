// first design for url validation, should be expanded
bool validateUrl(String url) {
  return Uri.tryParse(url)?.hasAbsolutePath ?? false;
}
