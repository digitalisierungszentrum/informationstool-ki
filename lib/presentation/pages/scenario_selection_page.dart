import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:informationtool/cubit/selector_cubit.dart';
import 'package:informationtool/cubit/selector_page_navigator_cubit.dart';
import 'package:informationtool/cubit/settings_cubit.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/injection.dart';
import 'package:informationtool/presentation/elements/custom_app_bar.dart';
import 'package:informationtool/presentation/elements/bottom_nav_bar/custom_bottom_bar.dart';
import 'package:informationtool/presentation/elements/return_information.dart';
import 'package:informationtool/presentation/elements/selection/scenario_selection_footer_widget.dart';
import 'package:informationtool/presentation/elements/selection/scenario_selection_list_widget.dart';
import 'package:informationtool/toolbox/context_extensions.dart';

class ScenarioSelectionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final PageController pageController = PageController();
    final SelectorCubit selectorCubit = getIt<SelectorCubit>();
    final DataSource dataSource = context
        .bloc<SettingsCubit>()
        .getCurrentDataSource()
        .getOrElse(() => const DataSource.local());
    selectorCubit.loadScenarios(dataSource);

    return WillPopScope(
      onWillPop: () async {
        showDialog(
          context: context,
          builder: (context) => ReturnInformationWidget(),
        );
        return false;
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider<SelectorCubit>(
            create: (context) => selectorCubit,
          ),
          BlocProvider<SelectorPageNavigatorCubit>(
            create: (context) => getIt<SelectorPageNavigatorCubit>(),
          )
        ],
        child: Scaffold(
          appBar: CustomAppBar(context.loc.selections_title),
          body: Column(
            children: [
              Expanded(
                child: ScenarioSelectionListWidget(pageController),
              ),
              ScenarioSelectionFooterWidget(pageController),
            ],
          ),
          bottomNavigationBar: CustomBottomBar(),
        ),
      ),
    );
  }
}
