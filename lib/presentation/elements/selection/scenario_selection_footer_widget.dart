import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:informationtool/app_router.gr.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/cubit/selector_cubit.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/logger.util.dart';
import 'package:informationtool/toolbox/context_extensions.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ScenarioSelectionFooterWidget extends StatelessWidget {
  final PageController _pageController;
  final log = getLogger();

  ScenarioSelectionFooterWidget(this._pageController);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SelectorCubit, SelectorState>(
      builder: (context, state) => _buildScenarioSelectionFooterWidget(
        context,
        context.bloc<SelectorCubit>().state,
      ),
    );
  }

  Widget _buildScenarioSelectionFooterWidget(
    BuildContext context,
    SelectorState state,
  ) {
    return state.map(
      initial: (_) => Container(),
      loading: (_) => Container(),
      modifing: (_) => Container(),
      loaded: (loaded) => _buildFooterWidget(
        context,
        loaded.uiScenarioSelectors.length,
        loaded.scenarios,
      ),
      modified: (modified) => _buildFooterWidget(
        context,
        modified.uiScenarioSelectors.length,
        modified.scenarios,
      ),
    );
  }

  Widget _buildFooterWidget(
    BuildContext context,
    int numSelectors,
    List<Scenario> filteredScenarios,
  ) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            if (numSelectors > 0)
              SmoothPageIndicator(
                controller: _pageController,
                count: numSelectors,
                effect: const WormEffect(
                  activeDotColor: blue2,
                  dotColor: blue3,
                ),
              ),
            const Divider(),
            Padding(
              padding: const EdgeInsets.all(20),
              child: ElevatedButton(
                onPressed: () => _navigateToResults(context, filteredScenarios),
                child: Text(
                  "${filteredScenarios.length} ${context.loc.selections_show_results}",
                  style: const TextStyle(fontSize: fontSmall),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _navigateToResults(
    BuildContext context,
    List<Scenario> filteredScenarios,
  ) {
    for (final element in filteredScenarios) {
      log.d("${element.name}: ${element.matchingHitRatio}");
    }

    context.router.push(
      ResultRoute(
        // sort scenarios by hit ratio descending
        scenarios: filteredScenarios.toList()
          ..sort(
            ((a, b) => b.matchingHitRatio!.compareTo(a.matchingHitRatio!)),
          ),
      ),
    );
  }
}
