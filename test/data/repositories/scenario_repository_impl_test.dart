import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:informationtool/data/datasources/assets_data_source.dart';
import 'package:informationtool/data/repositories/scenario_repository_impl.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/toolbox/either_extensions.dart';
import 'package:mocktail/mocktail.dart';

class MockAssetDataSource extends Mock implements AssetsDataSource {}

void main() {
  late AssetsDataSource assets;

  setUp(() {
    assets = MockAssetDataSource();
  });

  void executeFailureTest(
    Future<Either<Failure, List<Scenario>>> Function() getScenariosFunction,
    String expectedErrorMessage,
  ) {
    test("It should return a failure, if the json text is empty", () async {
      // assign
      when(() => assets.getAssetString(any()))
          .thenAnswer((_) => Future.value(''));

      // act
      final data = await getScenariosFunction();

      // assert
      expect(data.isLeft(), isTrue);
      data.fold(
        (l) => expect(l.errorCode, expectedErrorMessage),
        // this is always false, because the data above should return an error
        (r) => expect(0, 1),
      );
    });
  }

  void executeSuccessTest(
    Future<Either<Failure, List<Scenario>>> Function() getScenariosFunction,
  ) {
    test(
      "It should return the parsed json if the json text is correct",
      () async {
        // assign
        final assetContent =
            File("test/data/repositories/assets.json").readAsString();
        when(() => assets.getAssetString(any()))
            .thenAnswer((_) => assetContent);

        // act
        final data = await getScenariosFunction();

        // assert
        expect(data.isRight(), isTrue);
        expect(data.getNullableRight()!.length, 1);
        expect(data.getNullableRight()!.first.name, "Test");
        expect(
          data.getNullableRight()!.first.description,
          "Test description",
        );
        expect(data.getNullableRight()!.first.links.length, 1);
        expect(data.getNullableRight()!.first.links.first.name, "Test link");
        expect(
          data.getNullableRight()!.first.links.first.hyperlink,
          "https://test.com",
        );
        expect(
          data.getNullableRight()!.first.links.first.description,
          "Test link description",
        );
        expect(data.getNullableRight()!.first.picture, "test.png");
        expect(data.getNullableRight()!.first.selectors.length, 1);
        expect(
          data.getNullableRight()!.first.selectors.first.name,
          "test selector",
        );
        expect(
          data.getNullableRight()!.first.selectors.first.values,
          containsAllInOrder({"test1", "test2"}),
        );
      },
    );
  }

  group(
    "getScenarios",
    () {
      executeFailureTest(
        () => ScenarioRepositoryImpl(assets, assets).getLocalScenarios(),
        "error-loading-local-json",
      );
      executeFailureTest(
        () => ScenarioRepositoryImpl(assets, assets)
            .getRemoteScenarios("test-url"),
        "error-loading-remote-json",
      );

      executeSuccessTest(
        () => ScenarioRepositoryImpl(assets, assets).getLocalScenarios(),
      );
      executeSuccessTest(
        () => ScenarioRepositoryImpl(assets, assets)
            .getRemoteScenarios("test-url"),
      );
    },
  );
}
