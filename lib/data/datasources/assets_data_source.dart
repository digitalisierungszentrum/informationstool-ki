abstract class AssetsDataSource{
  Future<String> getAssetString(String assetPath);
}
