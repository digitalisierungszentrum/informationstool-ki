import argparse
import dataclasses
import json
from typing import List

import numpy as np
import pandas as pd


@dataclasses.dataclass
class Link:
    """class representing a link with name, hyperlinkg and description"""
    name: str
    hyperlink: str
    description: str


@dataclasses.dataclass
class Selector:
    """class representing one selector with multiple values"""
    name: str
    values: List[str]


@dataclasses.dataclass
class Scenario:
    """class representing one scenario"""
    name: str
    description: str
    links: List[Link]
    picture: str
    selectors: List[Selector]

    def add_link(self, link: Link):
        self.links.append(link)

    def add_selector(self, selector: Selector):
        self.selectors.append(selector)


class EnhancedJSONEncoder(json.JSONEncoder):
    """utility class allowing JSON serialization of dataclass objects"""

    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)


def parse_scenarios(scenario_data: pd.DataFrame):
    scenario_items = []
    for idx, row in scenario_data.iterrows():
        name, description, picture, hyperlink, link_name, link_description, selector_name, selector_values = row
        link = Link(link_name, hyperlink, link_description) if hyperlink else None
        selector = Selector(selector_name, [s.strip() for s in
                                            selector_values.split(",")]) if selector_name else None
        if name:
            new_scenario = Scenario(name, description, [link], picture, [selector])
            scenario_items.append(new_scenario)
        else:
            if not scenario_items:
                raise ValueError("unexpectedly encountered an incomplete row")
            last_scenario = scenario_items[-1]
            if link:
                last_scenario.add_link(link)
            if selector:
                last_scenario.add_selector(selector)

    return scenario_items


def main(input_file_name: str):
    data = pd.read_excel(input_file_name)
    data = data.replace(np.nan, None)
    scenario_items = parse_scenarios(data)
    print(json.dumps(scenario_items, indent=2, ensure_ascii=False, cls=EnhancedJSONEncoder))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Scenario Parser',
        description='This tool parses an xlsx file with scenario data and outputs it in JSON format')
    parser.add_argument('input_file_name')
    args = parser.parse_args()
    main(args.input_file_name)
