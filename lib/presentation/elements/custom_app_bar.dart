// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:informationtool/app_router.gr.dart';
import 'package:informationtool/app_theme.dart';
import 'package:informationtool/presentation/elements/return_information.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  late final String? _title;
  late final Icon? _icon;
  late final VoidCallback _onIconClickMethod;

  CustomAppBar(this._title)
      : preferredSize = const Size.fromHeight(fontMedium + 10.0) {
    _icon = null;
    _onIconClickMethod = () {};
  }

  CustomAppBar.withMethod(this._title, this._onIconClickMethod)
      : preferredSize = const Size.fromHeight(fontMedium + 10.0) {
    _icon = null;
  }

  CustomAppBar.iconOnly(this._icon, this._onIconClickMethod)
      : preferredSize = const Size.fromHeight(fontMedium + 10.0) {
    _title = null;
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        _title??"",
        style: const TextStyle(
          fontSize: fontSmall,
        ),
      ),
      centerTitle: true,
      leadingWidth: fontMedium *
          3, // leadingWith has to be changed to remove overflow of leading icons (return and dz icon)
      titleSpacing: 0,
      leading: _buildLeading(context),
      actions: _buildActionIcon(context),
    );
  }

  Widget _buildLeading(BuildContext context) {
    if (_title == null) {
      return Container();
    }

    return Row(
      children: [
        Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              size: fontMedium,
            ),
            onPressed: () => _return(context),
          ),
        ),
        SvgPicture.asset(
          "assets/dz_logo.svg",
          height: fontSmall,
        ),
      ],
    );
  }

  List<Widget> _buildActionIcon(BuildContext context) {
    if (_title != null) {
      return [];
    }

    return <Widget>[
      Padding(
        padding: const EdgeInsets.only(right: 20.0),
        child: IconButton(
          onPressed: _onIconClickMethod,
          icon: _icon!,
          iconSize: fontMedium,
        ),
      )
    ];
  }

  void _return(BuildContext context) {
    if (context.router.currentPath == const ScenarioSelectionRoute().path) {
      showDialog(
        context: context,
        builder: (context) => ReturnInformationWidget(),
      );
    } else {
      context.router.pop();
    }
  }
}
