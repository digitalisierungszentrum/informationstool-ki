import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:informationtool/domain/entities/scenario.dart';
import 'package:informationtool/domain/entities/scenario_link.dart';
import 'package:informationtool/domain/entities/scenario_selector.dart';
import 'package:informationtool/domain/repositories/scenario_repository.dart';
import 'package:informationtool/domain/usecases/get_all_scenarios.dart';
import 'package:informationtool/toolbox/either_extensions.dart';
import 'package:mocktail/mocktail.dart';

class MockScenarioRepository extends Mock implements ScenarioRepository {}

void main() {
  late ScenarioRepository repository;

  setUp(() {
    repository = MockScenarioRepository();
  });

  void executeUsecaseTest(
    Future<Either<Failure, List<Scenario>>> Function() functionToMock,
    DataSource dataSource,
  ) {
    test("It should return the list of the repository", () async {
      // assign
      when(functionToMock).thenAnswer(
        (_) => Future.value(
          right([
            Scenario(
              name: "Test",
              description: "Test description",
              links: [
                ScenarioLink(
                  name: "Test link",
                  hyperlink: "https://test.com",
                  description: "Test link description",
                )
              ],
              picture: "test.png",
              selectors: [
                ScenarioSelector(
                  name: "test selector",
                  values: ["test1", "test2"],
                )
              ],
            )
          ]),
        ),
      );

      // act
      final data = await GetAllScenariosUsecase(repository)(dataSource);

      // assert
      expect(data.isRight(), isTrue);
      expect(data.getNullableRight()!.length, 1);
      expect(data.getNullableRight()!.first.name, "Test");
      expect(data.getNullableRight()!.first.description, "Test description");
      expect(data.getNullableRight()!.first.links.length, 1);
      expect(data.getNullableRight()!.first.links.first.name, "Test link");
      expect(
        data.getNullableRight()!.first.links.first.hyperlink,
        "https://test.com",
      );
      expect(
        data.getNullableRight()!.first.links.first.description,
        "Test link description",
      );
      expect(data.getNullableRight()!.first.picture, "test.png");
      expect(data.getNullableRight()!.first.selectors.length, 1);
      expect(
        data.getNullableRight()!.first.selectors.first.name,
        "test selector",
      );
      expect(
        data.getNullableRight()!.first.selectors.first.values,
        containsAllInOrder({"test1", "test2"}),
      );
    });
  }

  group("GetAllScenarios", () {
    executeUsecaseTest(
      () => repository.getLocalScenarios(),
      const DataSource.local(),
    );
    executeUsecaseTest(
      () => repository.getRemoteScenarios(any()),
      const DataSource.remote("test-url"),
    );
  });
}
