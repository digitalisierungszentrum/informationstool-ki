import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:informationtool/domain/entities/data_source.dart';
import 'package:informationtool/domain/entities/failure.dart';
import 'package:injectable/injectable.dart';

part 'settings_cubit.freezed.dart';
part 'settings_state.dart';

@lazySingleton
class SettingsCubit extends Cubit<SettingsState> {
  String? remoteUrl;

  SettingsCubit() : super(const SettingsState.initial());

  Future<void> setLocalDataSource() async {
    emit(SettingsState.loaded(const DataSource.local()));
  }

  Future<void> setUserEntryExpected() async {
    emit(const SettingsState.loading());
  }

  Future<void> setRemoteUrl(String remoteUrl) async {
    this.remoteUrl = remoteUrl;
    emit(SettingsState.loaded(DataSource.remote(remoteUrl)));
  }

  Either<Failure, DataSource> getCurrentDataSource() {
    final curState = state;

    if (curState is Loaded) {
      return right(curState.dataSource);
    } else if (curState is Initial) {
      return right(curState.dataSource);
    } else {
      return left(const Failure("Unsupported settings state"));
    }
  }
}
